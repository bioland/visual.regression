# visual.regresso Starterkit

## Prerequisites

Follow instructions for installing visual.regresso https://github.com/eaudeweb/visual.regresso.git.

This should expose the `[DEBUG_LEVEL=INFO/DEBUG] visual-regresso [compare/history]` command, which you can run in this folder to generate comparison screenshots.

